function prompt_sep
    if test "$fish_key_bindings" = "fish_vi_key_bindings"
        or test "$fish_key_bindings" = "fish_hybrid_key_bindings"
        switch $fish_bind_mode
            case default
                set_color black
            case insert
                set_color green
            case replace_one
                set_color blue
            case visual
                set_color yellow
        end
    else
        set_color cyan
    end
    echo '>'
end

function fish_prompt
	echo -ns (set_color -o) (prompt_sep) (set_color normal) ' '
end

function fish_right_prompt
	echo -ns (set_color black) (prompt_pwd) (__fish_git_prompt ":%s") (set_color normal)
end

function fish_mode_prompt
    # nothing here...
end

fish_vi_key_bindings

set __fish_git_prompt_showdirtystate
set __fish_git_prompt_showupstream "informative"
set __fish_git_prompt_showuntrackedfiles

set __fish_git_prompt_char_stateseparator ''
set __fish_git_prompt_char_upstream_prefix ' '
set __fish_git_prompt_char_untrackedfiles '!'

function c
    cd $argv && clear && ls
end

# set PATH /home/max/bin $PATH
