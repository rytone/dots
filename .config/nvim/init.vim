call plug#begin('~/.local/share/nvim/plugged')

Plug 'unblevable/quick-scope'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-sleuth'

Plug 'w0rp/ale'
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'junegunn/fzf.vim'

Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'

Plug 'ElmCast/elm-vim'
Plug 'cespare/vim-toml'
Plug 'cstrahan/vim-capnp'
Plug 'dag/vim-fish'
Plug 'tikhomirov/vim-glsl'

call plug#end()

command! -bang -nargs=* Rg
	\ call fzf#vim#grep(
	\    'rg --column --line-number --no-heading --color=always --ignore-case '.shellescape(<q-args>), 1,
	\    <bang>0 ? fzf#vim#with_preview('up:60%')
	\            : fzf#vim#with_preview('right:50%:hidden', '?'),
	\    <bang>0)

let g:ale_linters = {'rust': []}

let g:ale_fixers = {'rust': ['rustfmt']}
let g:ale_fix_on_save = 1

function! CocCurrentFunction()
	return get(b:, 'coc_current_function', '')
endfunction

let g:lightline = {
	\ 'active': {
	\	'right': [
	\		['lineinfo'],
	\		['percent'],
	\		['cocstatus', 'currentfunction', 'linter_checking', 'linter_errors', 'linter_warnings', 'fileformat', 'fileencoding'],
	\	],
	\ },
	\
	\ 'component_function': {
	\	'cocstatus': 'coc#status',
	\	'currentfunction': 'CocCurrentFunction',
	\ },
	\ 'component_expand': {
	\	'linter_checking': 'lightline#ale#checking',
	\	'linter_warnings': 'lightline#ale#warnings',
	\	'linter_errors': 'lightline#ale#errors',
	\ },
	\ 'component_type': {
	\	'linter_checking': 'left',
	\	'linter_warnings': 'warning',
	\	'linter_errors': 'error',
	\ },
	\
	\ 'colorscheme': 'palm',
	\ }

set number
set relativenumber

set colorcolumn=80
highlight ColorColumn ctermbg=8

set background="light"

set autoread

set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4

hi Pmenu ctermbg=none ctermfg=none
hi PmenuSel ctermbg=2 ctermfg=231
hi PmenuSbar ctermbg=none
hi PmenuThumb ctermbg=7

hi SpellCap ctermbg=195
hi SpellBad ctermbg=174

inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<TAB>"
