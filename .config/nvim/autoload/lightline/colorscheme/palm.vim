let s:background = '#fdfffc'
let s:foreground = '#212121'

let s:black =   '#d1d3d2'
let s:red =     '#a85751'
let s:green =   '#95b29f'
let s:yellow =  '#d7816a'
let s:blue =    '#0892a5'
let s:magenta = '#c97b84'
let s:cyan =    '#4e937a'
let s:white =   '#463730'

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left = [ [ '#ffffff', s:cyan, 231, 6, 'bold' ], [ '#ffffff', s:green, 231, 2, 'bold' ] ]
let s:p.normal.middle = [ [ s:black, s:background, 0, 'none', 'bold' ] ]
let s:p.normal.right = [ [ s:blue, s:background, 4, 'none', 'bold' ], [ s:blue, s:background, 4, 'none', 'bold' ] ]

let s:p.insert.left = [ [ '#ffffff', s:magenta, 231, 5, 'bold' ], [ '#ffffff', s:yellow, 231, 3, 'bold' ] ]
let s:p.visual.left = [ [ '#ffffff', s:red, 231, 1, 'bold' ], [ '#ffffff', s:magenta, 231, 5, 'bold' ] ]
let s:p.replace.left = [ [ '#ffffff', s:red, 231, 1, 'bold' ], [ '#ffffff', s:yellow, 231, 3, 'bold' ] ]

let s:p.inactive.left =  [ [ s:cyan, s:background, 6, 'none' ], [ s:green, s:background, 2, 'none' ] ]
let s:p.inactive.middle = [ [ s:green, s:background, 2, 'none' ] ]
let s:p.inactive.right = [ [ s:green, s:background, 2, 'none' ], [ s:green, s:background, 2, 'none' ] ]

let s:p.tabline.left = [ [ s:cyan, s:background, 6, 'none' ] ]
let s:p.tabline.tabsel = [ [ s:cyan, s:background, 6, 'none', 'bold' ] ]
let s:p.tabline.middle = copy(s:p.normal.middle)
let s:p.tabline.right = copy(s:p.normal.right)

let s:p.normal.error = [ [ '#ffffff', s:red, 231, 1, 'bold' ] ]
let s:p.normal.warning = [ [ '#ffffff', s:yellow, 231, 3, 'bold' ] ]

let g:lightline#colorscheme#palm#palette = s:p
